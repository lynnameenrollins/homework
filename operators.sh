#! /bin/bash

#this shellscript will demostrate the use of operators

#arithematic operators

a=100
b=5

echo "---Demonstration of arithmetic operators---"
#subtraction
sub=$(($a-$b))
echo "$a + $b = " $sub

#multiplication
multi=$(($a*$b))
echo  "$a * $b = " $multi

#relational operators

echo "---Demonstration of relational operators---"

#greater than

if [ $a -gt $b ]
then
        echo "using -gt: $a is greater than $b"
else
        echo "using -gt: $b is greater than $a"

fi
if [ $a -ne $b ]
then
   echo "using -ne: $a is not equal to $b"
else
   echo "using -ne: $a is equal to $b"
fi

#Boolean operators

echo "---Demonstration of Boolean operators---"

#logical negation
if [ $a != $b ]
then
   echo "logical negation: $a != $b : a is not equal to b"
else
   echo "logical negation: $a != $b: a is equal to b"
fi

#logcial and
if [ $a -lt 101 -a $b -gt 15 ]
then
   echo "using logical and: $a -lt 101 -a $b -gt 15 : true"
else
   echo "using logical and: $a -lt 101 -a $b -gt 15 : false"
fi

#logical or
if [ $a -lt 101 -o $b -gt 15 ]
then
   echo "using logical or: $a -lt 101 -o $b -gt 15 : returns true"
else
   echo "using logical or: $a -lt 101 -o $b -gt 15 : returns false"
fi

#String operators
echo "---Demonstration of string operators---"

str1="Hello"
str2="Lynn"

echo "String length of $str1: " ${#str1}

catString=$str1**$str2
echo "Concatenate string with stars between $str1 and $str2: " $catString


echo "---Demonstration of file test operators---"
#file test operators
file="hello.txt"

echo $file
if [ -r $file ]
then
		echo "File has read access"
	else
		echo "File does not have read access"
fi


if [ -w $file ]
then
                echo "File has write permission"
        else
                echo "File does not have write permission"
fi

if [ -x $file ]
then
                echo "File has execute permssion"
        else
                echo "File does not have execute permission"
fi


